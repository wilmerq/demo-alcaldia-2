import { LocalStorageService } from "./../../services/almacenamiento/local-storage.service";
import { Responsable } from "../../modelos/Responsable";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-encargado",
  templateUrl: "./encargado.component.html",
  styleUrls: ["./encargado.component.scss"]
})
export class EncargadoComponent implements OnInit {
  files: any = [];
  foto: string;
  responsableActual: Responsable;

  constructor(private almacenamiento: LocalStorageService) {}

  ngOnInit() {
    this.responsableActual = new Responsable();
    this.foto = undefined;
  }

  uploadFile(event) {
    console.log(event);
    for (let index = 0; index < event.length; index++) {
      const element = event[index];
      this.files.push(element.name);
      this.getBase64(event[index]);
    }
  }

  deleteAttachment(index) {
    this.files.splice(index, 1);
  }

  getBase64(file) {
    let me = this;
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function() {
      me.foto = reader.result.toString();
    };
    reader.onerror = function(error) {
      console.log("Error: ", error);
    };
  }

  guardarResponsable() {
    this.responsableActual.foto = this.foto;
    this.almacenar(this.responsableActual);
    this.ngOnInit();
  }

  almacenar(r: Responsable) {
    try {
      let listaResponsables = this.almacenamiento.getObject("responsables");
      if (listaResponsables == null) {
        let listaResponsablesTmp: Responsable[] = new Array();
        listaResponsablesTmp.push(r);
        this.almacenamiento.guardar("responsables", listaResponsablesTmp);
      } else {
        let listaResponsablesTmp: Responsable[] = new Array();
        listaResponsablesTmp = JSON.parse(listaResponsables);
        listaResponsablesTmp.push(r);
        this.almacenamiento.deleteObject("responsables");
        this.almacenamiento.guardar("responsables", listaResponsablesTmp);
      }
    } catch (error) {
      console.log(error);
    }
  }
}
