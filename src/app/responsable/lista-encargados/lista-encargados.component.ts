import { LocalStorageService } from "./../../services/almacenamiento/local-storage.service";
import { Component, OnInit } from "@angular/core";
import { Responsable } from "app/modelos/Responsable";

@Component({
  selector: "app-lista-encargados",
  templateUrl: "./lista-encargados.component.html",
  styleUrls: ["./lista-encargados.component.scss"]
})
export class ListaEncargadosComponent implements OnInit {
  constructor(private almacenamiento: LocalStorageService) {}

  responsablesLista: Responsable[];

  ngOnInit() {
    let responsablesTmp = this.almacenamiento.getObject("responsables");
    this.responsablesLista = JSON.parse(responsablesTmp);
  }
}
