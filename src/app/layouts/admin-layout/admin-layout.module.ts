import { MostrarPredioComponent } from "./../../predios/mostrar-predio/mostrar-predio.component";
import { CrearComponent } from "./../../predios/crear/crear.component";
import { ListaEncargadosComponent } from "../../responsable/lista-encargados/lista-encargados.component";
import { EncargadoComponent } from "../../responsable/encargado/encargado.component";
import { LeafletDirective } from "@asymmetrik/ngx-leaflet";
import { PintarZonaComponent } from "../../predios/pintar-zona/pintar-zona.component";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AdminLayoutRoutes } from "./admin-layout.routing";
import { DashboardComponent } from "../../dashboard/dashboard.component";
import { UserProfileComponent } from "../../user-profile/user-profile.component";
import { TableListComponent } from "../../table-list/table-list.component";
import { TypographyComponent } from "../../typography/typography.component";
import { NotificationsComponent } from "../../notifications/notifications.component";
import { UpgradeComponent } from "../../upgrade/upgrade.component";
import { LeafletDrawModule } from "@asymmetrik/ngx-leaflet-draw";
import { LeafletModule } from "@asymmetrik/ngx-leaflet";
import {
  MatButtonModule,
  MatInputModule,
  MatRippleModule,
  MatFormFieldModule,
  MatTooltipModule,
  MatSelectModule,
  MatGridListModule,
  MatDividerModule,
  MatSliderModule
} from "@angular/material";
import { DibujarZonaComponent } from "../../predios/dibujar-zona/dibujar-zona.component";
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    LeafletDrawModule,
    LeafletModule,
    MatGridListModule,
    MatDividerModule,
    MatSliderModule
  ],
  declarations: [
    DashboardComponent,
    UserProfileComponent,
    TableListComponent,
    TypographyComponent,
    NotificationsComponent,
    UpgradeComponent,
    DibujarZonaComponent,
    PintarZonaComponent,
    EncargadoComponent,
    ListaEncargadosComponent,
    CrearComponent,
    MostrarPredioComponent
  ]
})
export class AdminLayoutModule {}
