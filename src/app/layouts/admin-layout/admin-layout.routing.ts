import { MostrarPredioComponent } from "../../predios/mostrar-predio/mostrar-predio.component";
import { CrearComponent } from "../../predios/crear/crear.component";
import { EncargadoComponent } from "../../responsable/encargado/encargado.component";
import { PintarZonaComponent } from "../../predios/pintar-zona/pintar-zona.component";
import { DibujarZonaComponent } from "../../predios/dibujar-zona/dibujar-zona.component";
import { Routes } from "@angular/router";

import { DashboardComponent } from "../../dashboard/dashboard.component";
import { UserProfileComponent } from "../../user-profile/user-profile.component";
import { TableListComponent } from "../../table-list/table-list.component";
import { TypographyComponent } from "../../typography/typography.component";
import { NotificationsComponent } from "../../notifications/notifications.component";
import { UpgradeComponent } from "../../upgrade/upgrade.component";

export const AdminLayoutRoutes: Routes = [
  { path: "dashboard", component: PintarZonaComponent },
  { path: "dibujar", component: DibujarZonaComponent },
  { path: "encargados", component: EncargadoComponent },
  { path: "crearPredio/:entrada", component: CrearComponent },
  { path: "mostrarPredio/:entrada", component: MostrarPredioComponent }
];
