import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { RouterModule } from "@angular/router";

import { LeafletDrawModule } from "@asymmetrik/ngx-leaflet-draw";
import { AppRoutingModule } from "./app.routing";
import { ComponentsModule } from "./components/components.module";

import { AppComponent } from "./app.component";
import { LeafletModule } from "@asymmetrik/ngx-leaflet";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { UserProfileComponent } from "./user-profile/user-profile.component";
import { TableListComponent } from "./table-list/table-list.component";
import { TypographyComponent } from "./typography/typography.component";
import { NotificationsComponent } from "./notifications/notifications.component";
import { UpgradeComponent } from "./upgrade/upgrade.component";
import { AdminLayoutComponent } from "./layouts/admin-layout/admin-layout.component";
import { DibujarZonaComponent } from "./predios/dibujar-zona/dibujar-zona.component";
import { PintarZonaComponent } from "./predios/pintar-zona/pintar-zona.component";
import { LeafletDirective } from "@asymmetrik/ngx-leaflet";
import { EncargadoComponent } from "./responsable/encargado/encargado.component";
import { ListaEncargadosComponent } from "./responsable/lista-encargados/lista-encargados.component";
import { CrearComponent } from "./predios/crear/crear.component";
import { DragDropDirectiveDirective } from "./directivas/drag-drop-directive.directive";
import { MostrarPredioComponent } from "./predios/mostrar-predio/mostrar-predio.component";

@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    ComponentsModule,
    RouterModule,
    AppRoutingModule,
    LeafletModule,
    LeafletDrawModule
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    DragDropDirectiveDirective
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
