import { Router } from "@angular/router";
import { CrearComponent } from "../crear/crear.component";
import { LocalStorageService } from "../../services/almacenamiento/local-storage.service";
import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewChecked,
  Renderer2
} from "@angular/core";
import { LeafletModule, LeafletUtil } from "@asymmetrik/ngx-leaflet";
import { tileLayer, latLng } from "leaflet";
import * as L from "leaflet";
import * as D from "leaflet-draw";

@Component({
  selector: "app-dibujar-zona",
  templateUrl: "./dibujar-zona.component.html",
  styleUrls: ["./dibujar-zona.component.scss"]
})
export class DibujarZonaComponent implements OnInit, AfterViewChecked {
  @ViewChild("div", { read: ElementRef, static: false })
  elementView: ElementRef;
  @ViewChild("map", { read: ElementRef, static: false })
  elementView2: ElementRef;

  options = {
    layers: [
      tileLayer("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
        maxZoom: 20,
        attribution: "..."
      })
    ],
    zoom: 18,
    center: latLng(11.007567, -74.245632),
    controls: {
      draw: {
        toolbar: {
          buttons: {
            polygon: "hola"
          }
        }
      }
    }
  };

  drawOptions = {
    position: "topleft",
    draw: {
      marker: false,
      polyline: false,
      circle: false,
      rectangle: false,
      circlemarker: false,
      polygon: {
        showArea: true,
        showLength: true
      }
    }
  };

  constructor(
    private almacenamiento: LocalStorageService,
    private router: Router,
    private renderer: Renderer2
  ) {}

  ngOnInit() {}

  ngAfterViewChecked(): void {
    let height = this.elementView.nativeElement.offsetHeight;
    this.renderer.setStyle(
      this.elementView2.nativeElement,
      "height",
      height + "px"
    );
  }

  zonaCreada(event: any) {
    let event1 = event.layer;
    console.log(event.layer);
    console.log(event1.toGeoJSON());
    //this.almacenamiento.guardar("poligono", event1.toGeoJSON());

    this.router.navigate(["crearPredio", JSON.stringify(event1.toGeoJSON())]);
  }
}
