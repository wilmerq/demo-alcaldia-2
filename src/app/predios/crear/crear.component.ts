import { Responsable } from "./../../modelos/Responsable";
import { LocalStorageService } from "./../../services/almacenamiento/local-storage.service";
import { Predio } from "./../../modelos/Predio";
import { ActivatedRoute } from "@angular/router";
import { Polygon, Layer } from "leaflet";
import { Component, OnInit, Input } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: "app-crear",
  templateUrl: "./crear.component.html",
  styleUrls: ["./crear.component.scss"]
})
export class CrearComponent implements OnInit {
  entrada: any;
  predioActual: Predio;
  myForm: FormGroup;
  responsables: Responsable[];

  constructor(
    private route: ActivatedRoute,
    public fb: FormBuilder,
    private almacenamiento: LocalStorageService
  ) {}

  ngOnInit() {
    this.predioActual = new Predio();
    this.entrada = JSON.parse(this.route.snapshot.paramMap.get("entrada"));
    this.predioActual.coordenas = this.normalizarLatLong(
      this.entrada.geometry.coordinates[0]
    );
    this.instanciarForm();
    this.responsables = JSON.parse(
      this.almacenamiento.getObject("responsables")
    );
  }

  guardarPredio() {
    this.predioActual.numero = this.myForm.get("numeroPredio").value;
    this.predioActual.direccion = this.myForm.get("direccion").value;
    this.predioActual.nombreTitular = this.myForm.get("titular").value;
    this.predioActual.responsable = this.myForm.get("responsable").value;
    this.predioActual.porcentaje = 0;
    this.almacenar(this.predioActual);
    this.ngOnInit();
  }

  almacenar(r: Predio) {
    try {
      let listaPredios = this.almacenamiento.getObject("predios");
      if (listaPredios == null) {
        let listaPrediosTmp: Predio[] = new Array();
        listaPrediosTmp.push(r);
        this.almacenamiento.guardar("predios", listaPrediosTmp);
      } else {
        let listaPrediosTmp: Predio[] = new Array();
        listaPrediosTmp = JSON.parse(listaPredios);
        listaPrediosTmp.push(r);
        this.almacenamiento.deleteObject("predios");
        this.almacenamiento.guardar("predios", listaPrediosTmp);
      }
    } catch (error) {
      console.log(error);
    }
  }

  normalizarLatLong(coordenas: Coordinates[]): Coordinates[] {
    let coordenasOrganizas: any[] = Array();
    for (let c of coordenas) {
      let coordenasTmp: any[] = Array();
      coordenasTmp[0] = c[1];
      coordenasTmp[1] = c[0];
      coordenasOrganizas.push(coordenasTmp);
    }
    return coordenasOrganizas;
  }

  instanciarForm() {
    this.myForm = this.fb.group({
      numeroPredio: ["", [Validators.required]],
      titular: ["", [Validators.required]],
      direccion: ["", [Validators.required]],
      responsable: [this.responsables, [Validators.required]]
    });
  }
}
