import { Predio } from "./../../modelos/Predio";
import { LocalStorageService } from "./../../services/almacenamiento/local-storage.service";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-mostrar-predio",
  templateUrl: "./mostrar-predio.component.html",
  styleUrls: ["./mostrar-predio.component.scss"]
})
export class MostrarPredioComponent implements OnInit {
  files: any = [];
  entrada: any;
  predioActual: Predio;

  formatLabel(value: number) {
    if (value >= 1) {
      return value + "%";
    }
    return value;
  }

  constructor(
    private route: ActivatedRoute,
    private almacenamiento: LocalStorageService
  ) {}

  ngOnInit() {
    this.entrada = JSON.parse(this.route.snapshot.paramMap.get("entrada"));
    this.buscarPredio();
  }

  uploadFile(event) {
    for (let index = 0; index < event.length; index++) {
      const element = event[index];
      this.files.push(element.name);
    }
  }

  deleteAttachment(index) {
    this.files.splice(index, 1);
  }

  descargarArchivo(index) {
    let url = window.URL.createObjectURL(this.files[index]);
    window.open(url);
  }

  buscarPredio() {
    let predios: Predio[] = JSON.parse(
      this.almacenamiento.getObject("predios")
    );
    for (let predio of predios) {
      if (this.compararCoordenadas(predio.coordenas, this.entrada[0])) {
        this.predioActual = predio;
        break;
      }
    }
  }

  compararCoordenadas(almacenadas: any[], entrantes: any[]): boolean {
    let iguales: any[] = Array();
    let size;
    if (almacenadas.length <= entrantes.length) {
      size = almacenadas.length;
    } else {
      size = entrantes.length;
    }
    for (let i = 0; i < size; i++) {
      let tmp: any[] = almacenadas[i];
      if (tmp[0] == entrantes[i].lat && tmp[1] == entrantes[i].lng) {
        iguales.push(true);
      } else {
        iguales.push(false);
      }
    }

    return iguales.includes(true);
  }
}
