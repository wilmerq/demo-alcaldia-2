import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MostrarPredioComponent } from './mostrar-predio.component';

describe('MostrarPredioComponent', () => {
  let component: MostrarPredioComponent;
  let fixture: ComponentFixture<MostrarPredioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MostrarPredioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MostrarPredioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
