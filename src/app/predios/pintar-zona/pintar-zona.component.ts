import { Predio } from "./../../modelos/Predio";
import {
  Component,
  OnInit,
  ElementRef,
  ViewChild,
  ViewChildren,
  AfterViewChecked,
  Renderer2,
  NgZone
} from "@angular/core";
import { tileLayer, latLng, circle, polygon, marker } from "leaflet";
import * as L from "leaflet";
import { LeafletDirective } from "@asymmetrik/ngx-leaflet";
import { LocalStorageService } from "../../services/almacenamiento/local-storage.service";
import { templateJitUrl } from "@angular/compiler";
import { Router } from "@angular/router";

@Component({
  selector: "app-pintar-zona",
  templateUrl: "./pintar-zona.component.html",
  styleUrls: ["./pintar-zona.component.scss"]
})
export class PintarZonaComponent implements OnInit, AfterViewChecked {
  @ViewChild("view", { read: ElementRef, static: false })
  elementView: ElementRef;
  @ViewChild("map", { read: ElementRef, static: false })
  elementView2: ElementRef;

  predios: Predio[];

  capasAdicionales: L.Layer[] = [
    polygon(
      [
        [46.8, -121.85],
        [46.92, -121.92],
        [46.87, -121.8]
      ],
      { bubblingMouseEvents: false, interactive: true }
    )
      .on(
        "click",
        function(e) {
          console.log("evento desde la capa x");
          console.log(this.label);
          console.log(e);
        },
        this
      )
      .on("mouseover", function(e) {
        console.log("mover mouse");
        let layer = e.target;
        layer.setStyle({
          weight: 2,
          color: "#666",
          fillColor: "white"
        });
      })
  ];

  options: L.MapOptions = {
    zoom: 15,
    center: latLng(11.005486, -74.244431),
    layers: [
      tileLayer("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
        maxZoom: 18
      })
    ]
  };

  constructor(
    private renderer: Renderer2,
    private almacenamiento: LocalStorageService,
    private router: Router,
    private ngZone: NgZone
  ) {}

  ngOnInit() {
    this.predios = JSON.parse(this.almacenamiento.getObject("predios"));
    this.crearCapas(this.predios);
  }

  ngAfterViewChecked(): void {
    let height = this.elementView.nativeElement.offsetHeight;
    this.renderer.setStyle(
      this.elementView2.nativeElement,
      "height",
      height + "px"
    );
  }

  crearCapas(predios: any) {
    for (let i of predios) {
      let color = "#666";
      switch (true) {
        case i.porcentaje < 15:
          color = "#FF0000";
          break;
        case i.porcentaje >= 15 && i.porcentaje < 30:
          color = "#FFFF00";
          break;
        case i.porcentaje >= 30 && i.porcentaje < 60:
          color = "#FF8000";
          break;
        case i.porcentaje >= 60 && i.porcentaje <= 100:
          color = "#009900";
          break;
        default:
          break;
      }
      this.capasAdicionales.push(
        polygon([i.coordenas], {
          bubblingMouseEvents: false,
          interactive: true,
          color: color
        })
          .on(
            "click",
            function(e) {
              console.log(JSON.stringify(e.target._latlngs));
              this.ngZone.run(() => {
                this.router.navigate([
                  "mostrarPredio",
                  JSON.stringify(e.target._latlngs)
                ]);
              });
            },
            this
          )
          .on("mouseover", function(e) {
            console.log("mover mouse");
            let layer = e.target;
            // layer.setStyle({
            //   weight: 2,
            //   color: "#666",
            //   fillColor: "white"
            // });
          })
      );
    }
  }

  groupPolygonos() {
    (L as any).ClusterablePolyline = L.Polyline.extend({
      _originalInitialize: (L as any).Polyline.prototype.initialize,

      initialize: function(bounds, options?) {
        this._originalInitialize(bounds, options);
        this._latlng = this.getBounds().getCenter();
      },

      getLatLng: function() {
        return this._latlng;
      },

      // dummy method.
      setLatLng: function() {}
    });

    (L as any).clusterablePolyline = function(latlngs, options?) {
      return new (L as any).ClusterablePolyline(latlngs, options);
    };

    (L as any).ClusterablePolygon = L.Polygon.extend({
      _originalInitialize: (L as any).Polygon.prototype.initialize,

      initialize: function(bounds, options?) {
        this._originalInitialize(bounds, options);
        this._latlng = this.getBounds().getCenter();
      },

      getLatLng: function() {
        return this._latlng;
      },

      // dummy method.
      setLatLng: function() {}
    });

    (L as any).clusterablePolygon = function(latlngs, options?) {
      return new (L as any).ClusterablePolygon(latlngs, options);
    };
  }
}
