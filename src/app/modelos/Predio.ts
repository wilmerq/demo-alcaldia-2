import { Responsable } from "./Responsable";
import { latLng } from "leaflet";
export class Predio {
  numero?: string;
  nombreTitular?: string;
  direccion?: string;
  coordenas?: Coordinates[];
  porcentaje?: number;
  responsable: Responsable;
}
