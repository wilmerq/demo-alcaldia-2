export class Responsable {
  nombre?: string;
  cedula?: number;
  email?: string;
  profesion?: string;
  celular?: number;
  foto?: string;
}
