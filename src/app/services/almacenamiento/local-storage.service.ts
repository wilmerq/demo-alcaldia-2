import { Responsable } from "./../../modelos/Responsable";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class LocalStorageService {
  constructor() {}

  guardar(key: string, objeto: any) {
    let temp: string = JSON.stringify(objeto);
    window.localStorage.setItem(key, temp);
  }

  getObject(key: string) {
    return window.localStorage.getItem(key);
  }

  deleteObject(key: string) {
    return window.localStorage.removeItem(key);
  }

  
}
